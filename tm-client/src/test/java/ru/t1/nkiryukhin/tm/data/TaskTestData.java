package ru.t1.nkiryukhin.tm.data;

import org.jetbrains.annotations.NotNull;

public class TaskTestData {

    @NotNull
    public final static String TASK_ONE_NAME = "Task 1";

    @NotNull
    public final static String TASK_ONE_DESCRIPTION = "Task One Description";

    @NotNull
    public final static String TASK_TWO_NAME = "Task 2";

    @NotNull
    public final static String TASK_TWO_DESCRIPTION = "Task Two Description";

    @NotNull
    public final static String TASK_THREE_NAME = "Task 3";

    @NotNull
    public final static String TASK_THREE_DESCRIPTION = "Task Three Description";

}