package ru.t1.nkiryukhin.tm.command.data;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.nkiryukhin.tm.dto.request.DataXmlLoadJaxBRequest;
import ru.t1.nkiryukhin.tm.enumerated.Role;
import ru.t1.nkiryukhin.tm.exception.AbstractException;

public class DataXmlLoadJaxBCommand extends AbstractDataCommand {

    @Override
    public @Nullable String getArgument() {
        return null;
    }

    @NotNull
    @Override
    public String getDescription() {
        return "Load data from xml file";
    }

    @NotNull
    @Override
    public String getName() {
        return "data-load-xml-jaxb";
    }

    @SneakyThrows
    @Override
    public void execute() throws AbstractException {
        System.out.println("[DATA LOAD XML]");
        @NotNull final DataXmlLoadJaxBRequest request = new DataXmlLoadJaxBRequest(getToken());
        getDomainEndpointClient().loadDataXmlJaxB(request);
    }

    @Override
    public @NotNull Role[] getRoles() {
        return new Role[]{Role.ADMIN};
    }
}
