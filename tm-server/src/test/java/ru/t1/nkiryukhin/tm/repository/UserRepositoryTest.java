package ru.t1.nkiryukhin.tm.repository;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import ru.t1.nkiryukhin.tm.api.repository.IUserRepository;
import ru.t1.nkiryukhin.tm.api.service.IPropertyService;
import ru.t1.nkiryukhin.tm.exception.AbstractException;
import ru.t1.nkiryukhin.tm.exception.field.AbstractFieldException;
import ru.t1.nkiryukhin.tm.exception.user.AccessDeniedException;
import ru.t1.nkiryukhin.tm.marker.UnitCategory;
import ru.t1.nkiryukhin.tm.model.User;
import ru.t1.nkiryukhin.tm.service.PropertyService;

import static ru.t1.nkiryukhin.tm.data.UserTestData.*;

@Category(UnitCategory.class)
public final class UserRepositoryTest {

    @NotNull
    private final IUserRepository repository = new UserRepository();

    @NotNull
    private final IPropertyService propertyService = new PropertyService();

    @Before
    public void before() {
        repository.add(USUAL_USER);
    }

    @After
    public void after() throws AccessDeniedException {
        repository.removeAll(USER_LIST);
    }

    @Test
    public void add() throws AbstractFieldException {
        Assert.assertNull(repository.add(NULL_USER));
        Assert.assertNotNull(repository.add(ADMIN_USER));
        @Nullable final User user = repository.findOneById(ADMIN_USER.getId());
        Assert.assertNotNull(user);
        Assert.assertEquals(ADMIN_USER, user);
    }

    @Test
    public void addMany() throws AbstractFieldException {
        Assert.assertNotNull(repository.add(ADMIN_USER_LIST));
        for (final User user : ADMIN_USER_LIST)
            Assert.assertEquals(user, repository.findOneById(user.getId()));
    }

    @Test
    public void set() {
        @NotNull final IUserRepository emptyRepository = new UserRepository();
        Assert.assertTrue(emptyRepository.findAll().isEmpty());
        emptyRepository.add(ADMIN_USER_LIST);
        emptyRepository.set(USER_LIST);
        Assert.assertEquals(USER_LIST, emptyRepository.findAll());
    }

    @Test
    public void findAll() {
        @NotNull final IUserRepository emptyRepository = new UserRepository();
        Assert.assertTrue(emptyRepository.findAll().isEmpty());
        emptyRepository.add(ADMIN_USER_LIST);
        Assert.assertEquals(ADMIN_USER_LIST, emptyRepository.findAll());
    }

    @Test
    public void existsById() {
        Assert.assertFalse(repository.existsById(NON_EXISTING_USER_ID));
        Assert.assertTrue(repository.existsById(USUAL_USER.getId()));
    }

    @Test
    public void findOneById() throws AbstractFieldException {
        Assert.assertNull(repository.findOneById(null));
        Assert.assertNull(repository.findOneById(NON_EXISTING_USER_ID));
        @Nullable final User user = repository.findOneById(USUAL_USER.getId());
        Assert.assertNotNull(user);
        Assert.assertEquals(USUAL_USER, user);
    }

    @Test
    public void findOneByIndex() throws AbstractFieldException {
        Assert.assertNull(repository.findOneByIndex(null));
        final int index = repository.findAll().indexOf(USUAL_USER);
        @Nullable final User user = repository.findOneByIndex(index);
        Assert.assertNotNull(user);
        Assert.assertEquals(USUAL_USER, user);
    }

    @Test
    public void clear() {
        @NotNull final IUserRepository emptyRepository = new UserRepository();
        Assert.assertTrue(emptyRepository.findAll().isEmpty());
        emptyRepository.add(ADMIN_USER_LIST);
        emptyRepository.clear();
        Assert.assertEquals(0, emptyRepository.getSize());
    }

    @Test
    public void remove() throws AbstractException {
        Assert.assertNull(repository.removeOne(null));
        @Nullable final User createdUser = repository.add(ADMIN_USER);
        @Nullable final User removedUser = repository.removeOne(createdUser);
        Assert.assertNotNull(removedUser);
        Assert.assertEquals(ADMIN_USER, removedUser);
        Assert.assertNull(repository.findOneById(ADMIN_USER.getId()));
    }

    @Test
    public void removeById() throws AbstractFieldException {
        Assert.assertNull(repository.removeById(null));
        Assert.assertNull(repository.removeById(NON_EXISTING_USER_ID));
        @Nullable final User createdUser = repository.add(ADMIN_USER);
        @Nullable final User removedUser = repository.removeById(ADMIN_USER.getId());
        Assert.assertNotNull(removedUser);
        Assert.assertEquals(ADMIN_USER, removedUser);
        Assert.assertNull(repository.findOneById(ADMIN_USER.getId()));
    }

    @Test
    public void removeByIndex() throws AbstractFieldException {
        Assert.assertNull(repository.removeByIndex(null));
        @Nullable final User createdUser = repository.add(ADMIN_USER);
        final int index = repository.findAll().indexOf(createdUser);
        @Nullable final User removedUser = repository.removeByIndex(index);
        Assert.assertNotNull(removedUser);
        Assert.assertEquals(ADMIN_USER, removedUser);
        Assert.assertNull(repository.findOneById(ADMIN_USER.getId()));
    }

    @Test
    public void getSize() {
        @NotNull final IUserRepository emptyRepository = new UserRepository();
        Assert.assertTrue(emptyRepository.findAll().isEmpty());
        Assert.assertEquals(0, emptyRepository.getSize());
        emptyRepository.add(ADMIN_USER);
        Assert.assertEquals(1, emptyRepository.getSize());
    }

    @Test
    public void findByLogin() {
        Assert.assertNull(repository.findByLogin(null));
        @Nullable final User user = repository.findByLogin(USUAL_USER.getLogin());
        Assert.assertNotNull(user);
        Assert.assertEquals(USUAL_USER, user);
    }

    @Test
    public void findByEmail() {
        Assert.assertNull(repository.findByEmail(null));
        @Nullable final User user = repository.findByEmail(USUAL_USER.getEmail());
        Assert.assertNotNull(user);
        Assert.assertEquals(USUAL_USER, user);
    }

    @Test
    public void removeAll() throws AccessDeniedException {
        @NotNull final IUserRepository emptyRepository = new UserRepository();
        Assert.assertTrue(emptyRepository.findAll().isEmpty());
        emptyRepository.add(USER_LIST);
        emptyRepository.removeAll(USER_LIST);
        Assert.assertEquals(0, emptyRepository.getSize());
    }

    @Test
    public void isLoginExists() {
        Assert.assertFalse(repository.isLoginExist(null));
        Assert.assertTrue(repository.isLoginExist(USUAL_USER.getLogin()));
    }

    @Test
    public void isEmailExists() {
        Assert.assertFalse(repository.isEmailExist(null));
        Assert.assertTrue(repository.isEmailExist(USUAL_USER.getEmail()));
    }

}