package ru.t1.nkiryukhin.tm.repository;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import ru.t1.nkiryukhin.tm.api.repository.IProjectRepository;
import ru.t1.nkiryukhin.tm.comparator.NameComparator;
import ru.t1.nkiryukhin.tm.exception.AbstractException;
import ru.t1.nkiryukhin.tm.exception.field.AbstractFieldException;
import ru.t1.nkiryukhin.tm.exception.field.UserIdEmptyException;
import ru.t1.nkiryukhin.tm.exception.user.AccessDeniedException;
import ru.t1.nkiryukhin.tm.marker.UnitCategory;
import ru.t1.nkiryukhin.tm.model.Project;

import java.util.Collections;
import java.util.Comparator;
import java.util.stream.Collectors;

import static ru.t1.nkiryukhin.tm.data.ProjectTestData.*;
import static ru.t1.nkiryukhin.tm.data.UserTestData.ADMIN_USER;
import static ru.t1.nkiryukhin.tm.data.UserTestData.USUAL_USER;

@Category(UnitCategory.class)
public final class ProjectRepositoryTest {

    @NotNull
    private final IProjectRepository repository = new ProjectRepository();

    @Before
    public void before() {
        repository.add(USUAL_PROJECT1);
        repository.add(USUAL_PROJECT2);
    }

    @After
    public void after() throws AccessDeniedException {
        repository.removeAll(PROJECT_LIST);
    }

    @Test
    public void add() throws AbstractFieldException {
        Assert.assertNull(repository.add(NULL_PROJECT));
        Assert.assertNotNull(repository.add(ADMIN_PROJECT1));
        @Nullable final Project project = repository.findOneById(ADMIN_PROJECT1.getId());
        Assert.assertNotNull(project);
        Assert.assertEquals(ADMIN_PROJECT1, project);
    }

    @Test
    public void addMany() throws AbstractFieldException {
        Assert.assertNotNull(repository.add(ADMIN_PROJECT_LIST));
        for (final Project project : ADMIN_PROJECT_LIST)
            Assert.assertEquals(project, repository.findOneById(project.getId()));
    }

    @Test
    public void addByUserId() throws AbstractFieldException {
        Assert.assertNull(repository.add(ADMIN_USER.getId(), NULL_PROJECT));
        Assert.assertNull(repository.add(null, ADMIN_PROJECT1));
        Assert.assertNotNull(repository.add(ADMIN_USER.getId(), ADMIN_PROJECT1));
        @Nullable final Project project = repository.findOneById(ADMIN_USER.getId(), ADMIN_PROJECT1.getId());
        Assert.assertNotNull(project);
        Assert.assertEquals(ADMIN_PROJECT1, project);
    }

    @Test
    public void createByUserId() throws AbstractFieldException {
        @NotNull final Project project = repository.create(ADMIN_USER.getId(), ADMIN_PROJECT1.getName());
        Assert.assertEquals(project, repository.findOneById(ADMIN_USER.getId(), project.getId()));
        Assert.assertEquals(ADMIN_PROJECT1.getName(), project.getName());
        Assert.assertEquals(ADMIN_USER.getId(), project.getUserId());
    }

    @Test
    public void createByUserIdWithDescription() throws AbstractFieldException {
        @NotNull final Project project = repository.create(ADMIN_USER.getId(), ADMIN_PROJECT1.getName(), ADMIN_PROJECT1.getDescription());
        Assert.assertEquals(project, repository.findOneById(ADMIN_USER.getId(), project.getId()));
        Assert.assertEquals(ADMIN_PROJECT1.getName(), project.getName());
        Assert.assertEquals(ADMIN_PROJECT1.getDescription(), project.getDescription());
        Assert.assertEquals(ADMIN_USER.getId(), project.getUserId());
    }

    @Test
    public void set() {
        @NotNull final IProjectRepository emptyRepository = new ProjectRepository();
        Assert.assertTrue(emptyRepository.findAll().isEmpty());
        emptyRepository.add(USUAL_PROJECT_LIST);
        emptyRepository.set(ADMIN_PROJECT_LIST);
        Assert.assertEquals(ADMIN_PROJECT_LIST, emptyRepository.findAll());
    }

    @Test
    public void findAll() {
        @NotNull final IProjectRepository emptyRepository = new ProjectRepository();
        Assert.assertTrue(emptyRepository.findAll().isEmpty());
        emptyRepository.add(USUAL_PROJECT_LIST);
        Assert.assertEquals(USUAL_PROJECT_LIST, emptyRepository.findAll());
    }

    @Test
    public void findAllByUserId() throws UserIdEmptyException {
        Assert.assertEquals(Collections.emptyList(), repository.findAll(""));
        Assert.assertEquals(USUAL_PROJECT_LIST, repository.findAll(USUAL_USER.getId()));
    }

    @Test
    public void findAllComparator() {
        @NotNull final IProjectRepository emptyRepository = new ProjectRepository();
        Assert.assertTrue(emptyRepository.findAll().isEmpty());
        emptyRepository.add(USUAL_PROJECT_LIST);
        emptyRepository.add(ADMIN_PROJECT_LIST);
        @NotNull final Comparator comparator = NameComparator.INSTANCE;
        Assert.assertEquals(SORTED_PROJECT_LIST, emptyRepository.findAll(comparator));
    }

    @Test
    public void findAllComparatorByUserId() throws UserIdEmptyException {
        @NotNull final Comparator comparator = NameComparator.INSTANCE;
        Assert.assertEquals(USUAL_PROJECT_LIST.stream().sorted(comparator).collect(Collectors.toList()), repository.findAll(USUAL_USER.getId(), comparator));
    }

    @Test
    public void existsById() {
        Assert.assertFalse(repository.existsById(NON_EXISTING_PROJECT_ID));
        Assert.assertTrue(repository.existsById(USUAL_PROJECT1.getId()));
    }

    @Test
    public void existsByIdByUserId() throws AbstractFieldException {
        Assert.assertFalse(repository.existsById(USUAL_USER.getId(), NON_EXISTING_PROJECT_ID));
        Assert.assertTrue(repository.existsById(USUAL_USER.getId(), USUAL_PROJECT1.getId()));
    }


    @Test
    public void findOneById() throws AbstractFieldException {
        Assert.assertNull(repository.findOneById(null));
        Assert.assertNull(repository.findOneById(NON_EXISTING_PROJECT_ID));
        @Nullable final Project project = repository.findOneById(USUAL_PROJECT1.getId());
        Assert.assertNotNull(project);
        Assert.assertEquals(USUAL_PROJECT1, project);
    }

    @Test
    public void findOneByIdByUserId() throws AbstractFieldException {
        Assert.assertNull(repository.findOneById(USUAL_USER.getId(), null));
        Assert.assertNull(repository.findOneById(USUAL_USER.getId(), NON_EXISTING_PROJECT_ID));
        @Nullable final Project project = repository.findOneById(USUAL_USER.getId(), USUAL_PROJECT1.getId());
        Assert.assertNotNull(project);
        Assert.assertEquals(USUAL_PROJECT1, project);
    }

    @Test
    public void findOneByIndex() throws AbstractFieldException {
        Assert.assertNull(repository.findOneByIndex(null));
        final int index = repository.findAll().indexOf(USUAL_PROJECT1);
        @Nullable final Project project = repository.findOneByIndex(index);
        Assert.assertNotNull(project);
        Assert.assertEquals(USUAL_PROJECT1, project);
    }

    @Test
    public void findOneByIndexByUserId() throws AbstractFieldException {
        Assert.assertNull(repository.findOneByIndex(USUAL_USER.getId(), null));
        final int index = repository.findAll().indexOf(USUAL_PROJECT1);
        @Nullable final Project project = repository.findOneByIndex(USUAL_USER.getId(), index);
        Assert.assertNotNull(project);
        Assert.assertEquals(USUAL_PROJECT1, project);
    }

    @Test
    public void clear() {
        @NotNull final IProjectRepository emptyRepository = new ProjectRepository();
        Assert.assertTrue(emptyRepository.findAll().isEmpty());
        emptyRepository.add(USUAL_PROJECT_LIST);
        emptyRepository.clear();
        Assert.assertEquals(0, emptyRepository.getSize());
    }

    @Test
    public void clearByUserId() throws UserIdEmptyException {
        @NotNull final IProjectRepository emptyRepository = new ProjectRepository();
        Assert.assertTrue(emptyRepository.findAll().isEmpty());
        emptyRepository.add(USUAL_PROJECT1);
        emptyRepository.add(USUAL_PROJECT2);
        emptyRepository.clear(USUAL_USER.getId());
        Assert.assertEquals(0, emptyRepository.getSize(USUAL_USER.getId()));
    }

    @Test
    public void remove() throws AbstractException {
        Assert.assertNull(repository.removeOne(null));
        @Nullable final Project createdProject = repository.add(ADMIN_PROJECT1);
        @Nullable final Project removedProject = repository.removeOne(createdProject);
        Assert.assertNotNull(removedProject);
        Assert.assertEquals(ADMIN_PROJECT1, removedProject);
        Assert.assertNull(repository.findOneById(ADMIN_PROJECT1.getId()));
    }

    @Test
    public void removeByUserId() throws AbstractFieldException {
        Assert.assertNull(repository.remove(ADMIN_USER.getId(), null));
        @Nullable final Project createdProject = repository.add(ADMIN_PROJECT1);
        Assert.assertNull(repository.remove(null, createdProject));
        @Nullable final Project removedProject = repository.remove(ADMIN_USER.getId(), createdProject);
        Assert.assertEquals(ADMIN_PROJECT1, removedProject);
        Assert.assertNull(repository.findOneById(ADMIN_USER.getId(), ADMIN_PROJECT1.getId()));
    }

    @Test
    public void removeById() throws AbstractFieldException {
        Assert.assertNull(repository.removeById(null));
        Assert.assertNull(repository.removeById(NON_EXISTING_PROJECT_ID));
        repository.add(ADMIN_PROJECT1);
        @Nullable final Project removedProject = repository.removeById(ADMIN_PROJECT1.getId());
        Assert.assertNotNull(removedProject);
        Assert.assertEquals(ADMIN_PROJECT1, removedProject);
        Assert.assertNull(repository.findOneById(ADMIN_PROJECT1.getId()));
    }

    @Test
    public void removeByIdByUserId() throws AbstractFieldException {
        Assert.assertNull(repository.removeById(ADMIN_USER.getId(), null));
        Assert.assertNull(repository.removeById(ADMIN_USER.getId(), NON_EXISTING_PROJECT_ID));
        Assert.assertNull(repository.removeById(ADMIN_USER.getId(), USUAL_PROJECT1.getId()));
        @Nullable final Project createdProject = repository.add(ADMIN_PROJECT1);
        Assert.assertNull(repository.removeById(null, createdProject.getId()));
        @Nullable final Project removedProject = repository.removeById(ADMIN_USER.getId(), createdProject.getId());
        Assert.assertNotNull(removedProject);
        Assert.assertEquals(ADMIN_PROJECT1, removedProject);
        Assert.assertNull(repository.findOneById(ADMIN_USER.getId(), ADMIN_PROJECT1.getId()));
    }

    @Test
    public void removeByIndex() throws AbstractFieldException {
        Assert.assertNull(repository.removeByIndex(null));
        @Nullable final Project createdProject = repository.add(ADMIN_PROJECT1);
        final int index = repository.findAll().indexOf(createdProject);
        @Nullable final Project removedProject = repository.removeByIndex(index);
        Assert.assertNotNull(removedProject);
        Assert.assertEquals(ADMIN_PROJECT1, removedProject);
        Assert.assertNull(repository.findOneById(ADMIN_PROJECT1.getId()));
    }

    @Test
    public void removeByIndexByUserId() throws AbstractFieldException {
        Assert.assertNull(repository.removeByIndex(ADMIN_USER.getId(), null));
        @Nullable final Project createdProject = repository.add(ADMIN_PROJECT1);
        final int index = repository.findAll(ADMIN_USER.getId()).indexOf(createdProject);
        Assert.assertNull(repository.removeByIndex(null, index));
        @Nullable final Project removedProject = repository.removeByIndex(ADMIN_USER.getId(), index);
        Assert.assertNotNull(removedProject);
        Assert.assertEquals(ADMIN_PROJECT1, removedProject);
        Assert.assertNull(repository.findOneById(ADMIN_USER.getId(), ADMIN_PROJECT1.getId()));
    }

    @Test
    public void getSize() {
        @NotNull final IProjectRepository emptyRepository = new ProjectRepository();
        Assert.assertTrue(emptyRepository.findAll().isEmpty());
        Assert.assertEquals(0, emptyRepository.getSize());
        emptyRepository.add(ADMIN_PROJECT1);
        Assert.assertEquals(1, emptyRepository.getSize());
    }

    @Test
    public void getSizeByUserId() throws UserIdEmptyException {
        @NotNull final IProjectRepository emptyRepository = new ProjectRepository();
        Assert.assertTrue(emptyRepository.findAll().isEmpty());
        Assert.assertEquals(0, emptyRepository.getSize(ADMIN_USER.getId()));
        emptyRepository.add(ADMIN_PROJECT1);
        emptyRepository.add(USUAL_PROJECT1);
        Assert.assertEquals(1, emptyRepository.getSize(ADMIN_USER.getId()));
    }

    @Test
    public void removeAll() throws AccessDeniedException {
        @NotNull final IProjectRepository emptyRepository = new ProjectRepository();
        Assert.assertTrue(emptyRepository.findAll().isEmpty());
        emptyRepository.add(PROJECT_LIST);
        emptyRepository.removeAll(PROJECT_LIST);
        Assert.assertEquals(0, emptyRepository.getSize());
    }

}