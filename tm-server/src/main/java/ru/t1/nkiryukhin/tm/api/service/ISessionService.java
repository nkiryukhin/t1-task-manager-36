package ru.t1.nkiryukhin.tm.api.service;

import ru.t1.nkiryukhin.tm.model.Session;

public interface ISessionService extends IUserOwnedService<Session> {
}